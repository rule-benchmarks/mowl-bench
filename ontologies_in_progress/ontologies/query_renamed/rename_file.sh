input_dir=$1

# Process each text file in the directory
for input_file in "$input_dir"/*.dlgp; do
  # Check if the file exists
  if [ -f "$input_file" ]; then
    # Read the first two lines of the file
    first_two_lines=$(head -n 2 "$input_file")

    # Extract the string inside the first <>
    new_name=$(echo "$first_two_lines" | grep -oP '(?<=/)[^/#]+(?=#)' | head -n 1  | tr -cd '[:alnum:]._-' | tr ' ' '_')

    # Check if the extraction was successful
    if [ -n "$new_name" ]; then
      # Construct the new filename with the same directory and .txt extension
      new_file_path="$input_dir/$new_name""_query_5.dlgp"

      # Rename the file
      cp "$input_file" "$new_file_path"

      echo "File $input_file renamed to $new_file_path"
    else
      echo "No string found inside <> in the first two lines of $input_file"
    fi
  else
    echo "No text files found in the directory $input_dir"
  fi
done
